// src/layout/AppFooter.tsx
import getConfig from 'next/config';
import React, { useContext } from 'react';
import { useLayoutContext } from '../context/layoutcontext';
import '@/styles/layout/layout.scss';
const AppFooter = () => {
    const { layoutConfig } = useLayoutContext();
    const contextPath = useLayoutContext()?.basePath;

    return (
        <div className="layout-footer">
            <img 
                src={`${contextPath}/layout/images/logo-${layoutConfig.colorScheme === 'light' ? 'dark' : 'white'}.svg`} 
                alt="Logo" 
                height="20" 
                className="mr-2" 
            />
            by
            <span className="font-medium ml-2">HCSBA</span>
        </div>
    );
};

export default AppFooter;
