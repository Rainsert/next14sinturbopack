// import { signOut, useSession } from 'next-auth/react';
import getConfig from 'next/config';
import Link from 'next/link';
import { classNames } from 'primereact/utils';
import React, { forwardRef, useContext, useImperativeHandle, useRef, useState, useEffect } from 'react';
import { useLayoutContext } from '@/context/layoutcontext';
import { Avatar } from 'primereact/avatar';
import '@/styles/layout/layout.scss';
// import { getPerfilByRut } from '@/service/perfiles';

const AppTopbar = forwardRef((props, ref) => {
    // const { data: session } = useSession();
    const { basePath, layoutConfig, layoutState, onMenuToggle, showProfileSidebar } = useLayoutContext();
    const menubuttonRef = useRef(null);
    const topbarmenuRef = useRef(null);
    const topbarmenubuttonRef = useRef(null);
    const perfilButtonRef = useRef(null);
    const [visibleDialogPerfil, setVisibleDialogPerfil] = useState(false);
    const [perfil, setPerfil] = useState({});

    // const contextPath = getConfig().publicRuntimeConfig.contextPath;

    useImperativeHandle(ref, () => ({
        menubutton: menubuttonRef.current,
        topbarmenu: topbarmenuRef.current,
        topbarmenubutton: topbarmenubuttonRef.current
    }));




    return (
        <div className="layout-topbar flex justify-content-between">
            <Link legacyBehavior href="/">
                <a className="layout-topbar-logo">
                    <div className="flex justify-content-around align-items-center col-12 h-5rem">
                        {/* <img src={`${contextPath}/layout/images/logo-${layoutConfig.colorScheme !== 'light' ? 'white' : 'dark'}.png`} alt="logo" className="h-6rem w-5rem" style={{ scale: '100%' }} /> */}
                        <span className="text-3xl font-italic font-bold">APA HCSBA</span>
                    </div>
                </a>
            </Link>

            <button ref={menubuttonRef} type="button" className="p-link layout-menu-button layout-topbar-button" onClick={onMenuToggle}>
                <i className="pi pi-bars" />
            </button>

            <button ref={topbarmenubuttonRef} type="button" className="p-link layout-topbar-menu-button layout-topbar-button" onClick={showProfileSidebar}>
                <i className="pi pi-ellipsis-v" />
            </button>

            <div
                ref={topbarmenuRef}
                className={classNames('layout-topbar-menu w-auto', {
                    'layout-topbar-menu-mobile-active': layoutState.profileSidebarVisible
                })}
            >
                <section className="col-12 flex flex-column md:flex-row md:justify-content-start pr-4">
                    {/* <div className="col-12 md:col-10 p-0 flex align-items-center justify-content-end md:align-items-start">
                        <h5 className="select-none h-full flex md:align-items-center justify-content-start md:justify-content-end">Hola, {session?.user?.name}</h5>
                         <button type="button" className="p-link layout-topbar-button w-auto" >
                            {traeFoto()}
                        </button> 
                    </div> */}
                    <div className="col-12 md:col-2 flex-column md:flex-row p-0 flex md:align-items-center md:justify-content-center md:ml-4">
                        <Link legacyBehavior href="/Mantenedores">
                            <button type="button" className="p-link layout-topbar-button h-min p-2 m-0">
                                <i className="pi pi-cog"></i>
                                <span>Mantenedores</span>
                            </button>
                        </Link>

                        {/* {session && (
                            <button type="button" onClick={() => signOut({ callbackUrl: '/login' })} className="p-link layout-topbar-button h-min p-2 m-0">
                                <i className="pi pi-sign-out"></i>
                                <span>Cerrar sesión</span>
                            </button>
                        )} */}
                    </div>
                </section>
            </div>
        </div>
    );
});

AppTopbar.displayName = 'AppTopbar';
export default AppTopbar;
