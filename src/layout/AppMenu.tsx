// src/layout/AppMenu.tsx
import React, { useContext } from 'react';
import { Menu } from 'primereact/menu';
import { useRouter, usePathname } from 'next/navigation';
import { useLayoutContext } from '../context/layoutcontext';
import '@/styles/layout/layout.scss';
const AppMenu = () => {
    const router = useRouter();
    const pathname = usePathname();
    const { layoutState } = useLayoutContext();

    const items = [
        {
            label: 'Home',
            icon: 'pi pi-fw pi-home',
            command: () => { router.push('/'); },
            className: pathname === '/index' ? 'active-menuitem' : '',
        },
        {
            label: 'AWA',
            icon: 'pi pi-fw pi-home',
            items: [
                {
                    label: 'uwu',
                    icon: 'pi pi-fw pi-file',
                    command: () => { router.push('/uwu'); },
                    className: pathname === '/uwu' ? 'active-menuitem' : '',
                },
                {
                    label: 'About',
                    icon: 'pi pi-fw pi-info',
                    command: () => { router.push('/about'); },
                    className: pathname === '/about' ? 'active-menuitem' : '',
                }
            ]
        }
    ];

    
    return (
        <div className={layoutState.staticMenuMobileActive ? 'layout-sidebar-active' : ''}>
            <Menu model={items} className="w-full border-0" />
        </div>
    );
};

export default AppMenu;
