// src/layout/AppSidebar.tsx
import React from 'react';
import AppMenu from './AppMenu';
import '@/styles/layout/layout.scss';
const AppSidebar = () => {
    return (
        <AppMenu />
    );
};

export default AppSidebar;