// src/layout/AppConfig.tsx
import React from 'react';
import { useLayoutContext } from '../context/layoutcontext';
import '@/styles/layout/layout.scss';
const AppConfig = () => {
    const { layoutConfig, setLayoutConfig } = useLayoutContext();

    const changeTheme = (theme: string) => {
        setLayoutConfig((prevConfig) => ({ ...prevConfig, theme }));
    };

    return (
        <div className="layout-config">
            {/* Aquí va la configuración del layout */}
            <button onClick={() => changeTheme('lara-light-indigo')}>Lara Light Indigo</button>
            <button onClick={() => changeTheme('lara-dark-indigo')}>Lara Dark Indigo</button>
        </div>
    );
};

export default AppConfig;
