'use client';

import getConfig from 'next/config';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEventListener, useUnmountEffect } from 'primereact/hooks';
import { classNames, DomHandler } from 'primereact/utils';
import React, { useContext, useEffect, useRef } from 'react';
import AppFooter from './AppFooter';
import AppSidebar from './AppSidebar';
import AppTopbar from './AppTopbar';
import AppConfig from './AppConfig';
import { useLayoutContext } from '../context/layoutcontext';
import PrimeReact from 'primereact/api';
import '@/styles/layout/layout.scss';

const Layout = ({ children }: { children: React.ReactNode }) => {
    const { layoutConfig, layoutState, setLayoutState } = useLayoutContext();
    const topbarRef = useRef<{ menubutton: HTMLButtonElement | null; topbarmenu: HTMLDivElement | null; topbarmenubutton: HTMLButtonElement | null } | null>(null);
    const sidebarRef = useRef<HTMLDivElement | null>(null);
    
    const contextPath = useLayoutContext()?.basePath;

    const router = useRouter();

    const [bindMenuOutsideClickListener, unbindMenuOutsideClickListener] = useEventListener({
        type: 'click',
        listener: (event) => {
            const isOutsideClicked = !(sidebarRef.current && (sidebarRef.current.isSameNode(event.target as Node) || sidebarRef.current.contains(event.target as Node)) ||
                topbarRef.current && topbarRef.current.menubutton && (topbarRef.current.menubutton.isSameNode(event.target as Node) || topbarRef.current.menubutton.contains(event.target as Node)));

            if (isOutsideClicked) {
                hideMenu();
            }
        }
    });

    const [bindProfileMenuOutsideClickListener, unbindProfileMenuOutsideClickListener] = useEventListener({
        type: 'click',
        listener: (event) => {
            const isOutsideClicked = !(topbarRef.current && topbarRef.current.topbarmenu && (topbarRef.current.topbarmenu.isSameNode(event.target as Node) || topbarRef.current.topbarmenu.contains(event.target as Node)) ||
                topbarRef.current && topbarRef.current.topbarmenubutton && (topbarRef.current.topbarmenubutton.isSameNode(event.target as Node) || topbarRef.current.topbarmenubutton.contains(event.target as Node)));

            if (isOutsideClicked) {
                hideProfileMenu();
            }
        }
    });

    const hideMenu = () => {
        setLayoutState((prevLayoutState) => ({ ...prevLayoutState, overlayMenuActive: false, staticMenuMobileActive: false, menuHoverActive: false }));
        unbindMenuOutsideClickListener();
        unblockBodyScroll();
    };

    const hideProfileMenu = () => {
        setLayoutState((prevLayoutState) => ({ ...prevLayoutState, profileSidebarVisible: false }));
        unbindProfileMenuOutsideClickListener();
    };

    const blockBodyScroll = () => {
        DomHandler.addClass(document.body, 'blocked-scroll');
    };

    const unblockBodyScroll = () => {
        DomHandler.removeClass(document.body, 'blocked-scroll');
    };

    useEffect(() => {
        if (layoutState.overlayMenuActive || layoutState.staticMenuMobileActive) {
            bindMenuOutsideClickListener();
        }

        layoutState.staticMenuMobileActive && blockBodyScroll();
    }, [layoutState.overlayMenuActive, layoutState.staticMenuMobileActive]);

    useEffect(() => {
        if (layoutState.profileSidebarVisible) {
            bindProfileMenuOutsideClickListener();
        }
    }, [layoutState.profileSidebarVisible]);

    useEffect(() => {
        const handleRouteChange = () => {
            hideMenu();
            hideProfileMenu();
        };

        router.events.on('routeChangeComplete', handleRouteChange);

        return () => {
            router.events.off('routeChangeComplete', handleRouteChange);
        };
    }, [router.events]);

    PrimeReact.ripple = true;

    useUnmountEffect(() => {
        unbindMenuOutsideClickListener();
        unbindProfileMenuOutsideClickListener();
    });

    const containerClass = classNames('layout-wrapper', {
        'layout-theme-light': layoutConfig.colorScheme === 'light',
        'layout-theme-dark': layoutConfig.colorScheme === 'dark',
        'layout-overlay': layoutConfig.menuMode === 'overlay',
        'layout-static': layoutConfig.menuMode === 'static',
        'layout-static-inactive': layoutState.staticMenuDesktopInactive && layoutConfig.menuMode === 'static',
        'layout-overlay-active': layoutState.overlayMenuActive,
        'layout-mobile-active': layoutState.staticMenuMobileActive,
        'p-input-filled': layoutConfig.inputStyle === 'filled',
        'p-ripple-disabled': !layoutConfig.ripple
    });

    return (
        <React.Fragment>
            <Head>
                <title>APA | HCSBA</title>
                <meta charSet="UTF-8" />
                <meta name="description" content="The ultimate collection of design-agnostic, flexible and accessible React UI Components." />
                <meta name="robots" content="index, follow" />
                <meta name="viewport" content="initial-scale=1, width=device-width" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="Sakai by PrimeReact | Free Admin Template for NextJS" />
                <meta property="og:url" content="https://www.primefaces.org/sakai-react" />
                <meta property="og:description" content="The ultimate collection of design-agnostic, flexible and accessible React UI Components." />
                <meta property="og:image" content="https://www.primefaces.org/static/social/sakai-nextjs.png" />
                <meta property="og:ttl" content="604800" />
                <link rel="icon" href={`${contextPath}/icons/HCSBA2.ico`} type="image/x-icon" />
            </Head>

            <div className={containerClass}>
                <AppTopbar ref={topbarRef} />
                <div ref={sidebarRef} className="layout-sidebar">
                    <AppSidebar />
                </div>
                <div className="layout-main-container">
                    <div className="layout-main">{children}</div>
                    <AppFooter />
                </div>
                <AppConfig />
                <div className="layout-mask"></div>
            </div>
        </React.Fragment>
    );
};

export default Layout;
