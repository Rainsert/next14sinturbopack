// src/pages/_app.tsx
import 'primereact/resources/themes/saga-blue/theme.css'; // Puedes cambiar el tema según prefieras
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css'; // Si has instalado primeflex
import '@/styles/layout/layout.scss';
import { LayoutProvider } from '../context/layoutcontext';
import Layout from '../layout/Layout';
import type { AppProps } from 'next/app';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <LayoutProvider>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </LayoutProvider>
  );
}

export default MyApp;
