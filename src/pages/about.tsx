// src/pages/about.tsx
import React from 'react';

const About: React.FC = () => {
  return (
    <div style={{ padding: '2rem' }}>
      <h1>About Page</h1>
      <p>This is the about page of the Next.js application.</p>
    </div>
  );
};

export default About;
