// src/context/AppContext.tsx
'use client';

import React, { createContext, useState, useContext, ReactNode, useEffect } from 'react';

interface AppContextProps {
  user: string;
  setUser: React.Dispatch<React.SetStateAction<string>>;
  basePath: string;
}

const AppContext = createContext<AppContextProps | undefined>(undefined);

export const AppProvider = ({ children }: { children: ReactNode }) => {
  const [user, setUser] = useState<string>('Ignacio');
  const [basePath, setBasePath] = useState<string>('');

  useEffect(() => {
    const path = localStorage.getItem('basePath') || process.env.NEXT_PUBLIC_BASE_PATH || '';
    setBasePath(path);
  }, []);

  const value = {
    user,
    setUser,
    basePath,
  };

  return <AppContext.Provider value={value}>{children}</AppContext.Provider>;
};

export const useAppContext = () => {
  const context = useContext(AppContext);
  return context;
};
